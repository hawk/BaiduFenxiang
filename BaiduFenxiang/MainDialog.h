#pragma once

#include "types.h"
#include "resource.h"
#include "PCS.h"
#include "FileTreeCtrl.h"

#define USE_FONT_NAME _T("MS Shell Dlg")
#define UM_RECOVER    ( WM_USER + 1)
#define UM_UPDATETREE ( WM_USER + 2)

// CMainDialog dialog
class CMainDialog : public CDialog
{
	DECLARE_DYNAMIC(CMainDialog)

public:
	CMainDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMainDialog();

// Dialog Data
	enum { IDD = IDD_MAIN_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // 实现
protected:
    HICON m_hIcon;

    // 生成的消息映射函数
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    virtual BOOL DestroyWindow();
    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedOk();

public:
    CTreeFileCtrl m_TreeFile;
    std::wstring   access_token;
    Setting        mSetting;
    std::vector<File> mFileList;
    FileTree mRoot;
    long mCurrentIndex;
    std::map<std::wstring, std::wstring> mMatchupRename;
    std::map<std::wstring, std::wstring> mMatchupRecover;
    std::vector<MoveItem> mMoveList;
    CFont *m_pFont;
    wchar_t m_sMapFileName[256];
    wchar_t m_rMapFileName[256];
    std::wstring m_sMarkName;
    void ReadSetting(void);
    void WriteSetting(void);
    void ShowAuthDialog(void);
    void walkdir(PCS *pcs, std::wstring dir, FileTree &current);
    void renameAll(PCS *pcs, FileTree current, OpMode mode);
    std::wstring constructNewName(PCS *pcs, std::wstring old, std::wstring &old_name, std::wstring &new_name, OpMode mode);
    std::wstring getCurrentIndexString(long &index);
    void insertToMap(std::wstring key, std::wstring value, OpMode mode);
    void saveMapFile(std::wstring mapFileName = _T("user.map"));
    void readMapFile(std::wstring mapFileName = _T("user.map"));
    long countFile(FileTree root);
    std::wstring genMapFileName(FileTree root);
    std::wstring getFileNameByPath(std::wstring path, BOOL leftBias);
    afx_msg void OnBnClickedButtonChangeAccount();
    afx_msg void OnBnClickedButtonApiTest();
    afx_msg void OnBnClickedButtonRename();
    afx_msg void OnBnClickedButtonRecover();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg LRESULT OnRecover(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnUpdateTree(WPARAM wParam, LPARAM lParam);
    afx_msg void OnBnClickedButtonRefresh();
    afx_msg void OnBnClickedButtonAddTail();
    afx_msg void OnBnClickedButtonRemoveTail();
    afx_msg void OnBnClickedButtonBlank();
    afx_msg void OnBnClickedButtonHelp();
    afx_msg void OnEnChangeEditInput();
};
