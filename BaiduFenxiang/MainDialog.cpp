// MainDialog.cpp : implementation file
//

#include "stdafx.h"
#include "MainDialog.h"
#include "HeiduDlg.h"
#include "WinHttpClient.h"
#include "PCS.h"

#pragma comment( lib, "comsupp.lib" )

#define DEFAULT_ROOT_PATH L"/apps/assistant/"

static const std::wstring seed = L"百度全球最大的中文搜索引擎最大的中文网站2000年1月创立于北京中关村1999\
年底身在美国硅谷的李彦宏看到了中国互联网及中文搜索引擎百度大厦服务的巨\
大发展潜力抱着技术改变世界的梦想他毅然辞掉硅谷的高薪工作携搜索引擎专利\
技术于2000年1月1日在中关村创建了百度公司从最初的不足10人发展至今员工人\
数超过18000人如今的百度已成为中国最受欢迎影响力最大的中文网站百度拥有\
数千名研发工程师这是中国乃至全球最为优秀的技术团队这支队伍掌握着世界上\
最为先进的搜索引擎技术使百度成为中国掌握世界尖端科学核心技术的中国高科\
技企业也使中国成为美国俄罗斯和韩国之外全球仅有的4个拥有搜索引擎核心技术\
的国家之一从创立之初百度便将让人们最平等便捷地获取信息找到所求作为自己的\
使命成立以来公司秉承以用户为导向的理念不断坚持技术创新致力于为用户提供简\
单可依赖的互联网搜索产品及服务其中包括以网络搜索为主的功能性搜索以贴吧为\
主的社区搜索针对各区域行业所需的垂直搜索Mp3搜索以及门户频道IM等全面覆盖了\
中文网络世界所有的搜索需求根据第三方权威数据百度在中国的搜索份额超过\
在面对用户的搜索产品不断丰富的同时百度还创新性地推出了基于搜索的营销推广\
服务并成为最受企业青睐的互联网营销推广平台如今中国已有数十万家企业使用了\
百度的搜索推广服务不断提升企业自身的品牌及运营效率通过持续的商业模式创新\
百度正进一步带动整个互联网行业和中小企业的经济增长推动社会经济的发展和转\
型为推动中国数百万中小网站的发展百度借助超大流量的平台优势联合所有优质的\
各类网站建立了世界上最大的网络联盟使各类企业的搜索推广品牌营销的价值覆盖\
面均大面积提升与此同时各网站也在联盟大家庭的互助下获得最大的生存与发展机会\
作为国内的一家知名企业百度也一直秉承弥合信息鸿沟共享知识社会的责任理念坚持\
履行企业公民的社会责任成立来百度利用自身优势积极投身公益事业先后投入巨大资\
源为盲人少儿老年人群体打造专门的搜索产品解决了特殊群体上网难问题,极大地弥补\
了社会信息鸿沟问题此外在加速推动中国信息化进程净化网络环境搜索引擎教育及提升\
大学生就业率等方面百度也一直走在行业领先的地位2011年初百度还特别成立了百度基\
金会围绕知识教育环境保护灾难救助等领域更加系统规范地管理和践行公益事业2005年\
百度在美国纳斯达克上市一举打破首日涨幅最高等多项纪录并成为首家进入纳斯达克成分\
股的中国公司通过数年来的市场表现百度优异的业绩与值得依赖的回报使之成为中国企业\
价值的代表傲然屹立于全球资本市场2009年百度推出全新的框计算技术概念并基于此理念\
推出百度开放平台帮助更多优秀的第三方开发者利用互联网平台自主创新自主创业在大幅\
提升网民互联网使用体验的同时带动起围绕用户需求进行研发的产业创新热潮对中国互联\
网产业的升级和发展产生巨大的拉动效应随着中国互联网从PC端向移动端转型百度也在积极\
围绕核心战略加大对移动和云领域的投入和布局不断把PC领域的优势向移动领域扩展在通\
过技术创新不断满足用户的移动搜索需求的同时百度也在继续积极推动移动云生态系统的\
建设和发展与产业实现共赢2012年9月百度面向开发者全面开放包括云存储大数据智能和云\
计算在内的核心云能力为开发者量身定制从开发到运营的七种武器为开发者提供更强大的技\
术运营支持与推广变现保障以帮助他们在移动云时代获得更好的收益和成长如今百度已经成\
为中国最具价值的品牌之一英国金融时报将百度列为中国十大世界级品牌成为这个榜单中最\
年轻的一家公司也是唯一一家互联网公司而亚洲最受尊敬企业全球最具创新力企业中国互联\
网力量之星等一系列荣誉称号的获得也无一不向外界展示着百度成立数年来的成就\
多年来百度董事长兼CEO李彦宏率领百度人所形成的简单可依赖的核心文化深深地植\
根于百度这是一个充满朝气求实坦诚的公司以搜索改变生活推动人类的文明与进步促\
进中国经济的发展为己任正朝着更为远大的目标而迈进";

const std::wstring hexChars = L"甲乙丙丁戊己庚辛子丑寅卯辰巳午未";

std::wstring getRandString( int index )
{
    int sSize = seed.size() / 10;
    srand(time(NULL));
    int num = rand() % sSize;
    
    return seed.substr(num, ( index + 1 ) % 7 );
}

std::wstring getTianGan(int index)
{
    wchar_t number[10];
    wsprintf(number, L"%X", index);

    std::wstring retn = L"";
    std::wstring sNumber = std::wstring(number);
    for( std::wstring::size_type i = 0; i < sNumber.size(); i++)
    {
        int offset = 0;
        wchar_t _char = sNumber.at(i);
        switch(_char)
        {
        case L'0':
        case L'1':
        case L'2':
        case L'3':
        case L'4':
        case L'5':
        case L'6':
        case L'7':
        case L'8':
        case L'9':
            offset = _char - L'0';
            break;
        case L'A':
        case L'B':
        case L'C':
        case L'D':
        case L'E':
        case L'F':
            offset = _char - L'A' + 10;
            break;
        default:
            break;
        }
    retn.push_back(hexChars.at(offset % hexChars.size()));
    }
    return retn;
}

class CAboutDlg: public CDialog
{
public:
    CAboutDlg();

    // 对话框数据
    enum
    {
        IDD = IDD_ABOUTBOX
    };

protected:
    virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV 支持

    // 实现
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg()
: CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// CHelpDlg dialog
class CHelpDlg: public CDialog
{
public:
    CHelpDlg();

    // 对话框数据
    enum
    {
        IDD = IDD_HELPBOX
    };

protected:
    virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV 支持

    // 实现
protected:
    DECLARE_MESSAGE_MAP()
};

CHelpDlg::CHelpDlg()
: CDialog(CHelpDlg::IDD)
{
}

void CHelpDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CHelpDlg, CDialog)
END_MESSAGE_MAP()

// CMainDialog dialog

IMPLEMENT_DYNAMIC(CMainDialog, CDialog)

CMainDialog::CMainDialog(CWnd* pParent /*=NULL*/)
: CDialog(CMainDialog::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_pFont = NULL;
}

CMainDialog::~CMainDialog()
{
    if(m_pFont != NULL)
    {
        delete m_pFont;
        m_pFont = NULL;
    }
}

void CMainDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMainDialog, CDialog)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDOK, &CMainDialog::OnBnClickedButtonBlank)
    ON_BN_CLICKED(IDC_BUTTON_CHANGE_ACCOUNT, &CMainDialog::OnBnClickedButtonChangeAccount)
    ON_BN_CLICKED(IDC_BUTTON_API_TEST, &CMainDialog::OnBnClickedButtonBlank)
    ON_BN_CLICKED(IDC_BUTTON_RENAME, &CMainDialog::OnBnClickedButtonBlank)
    ON_BN_CLICKED(IDC_BUTTON_RECOVER, &CMainDialog::OnBnClickedButtonBlank)
    ON_MESSAGE(UM_RECOVER, &CMainDialog::OnRecover)
    ON_MESSAGE(UM_UPDATETREE, &CMainDialog::OnUpdateTree)
    ON_WM_SIZE()
    ON_WM_CREATE()
    ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CMainDialog::OnBnClickedButtonRefresh)
    ON_BN_CLICKED(IDC_BUTTON_ADD_TAIL, &CMainDialog::OnBnClickedButtonAddTail)
    ON_BN_CLICKED(IDC_BUTTON_REMOVE_TAIL, &CMainDialog::OnBnClickedButtonRemoveTail)
    ON_BN_CLICKED(IDC_BUTTON_HELP, &CMainDialog::OnBnClickedButtonHelp)
    ON_EN_CHANGE(IDC_EDIT_REPLACE_CONTENT, &CMainDialog::OnEnChangeEditInput)
END_MESSAGE_MAP()

// CMainDialog message handlers

BOOL CMainDialog::OnInitDialog()
{

    CDialog::OnInitDialog();

    // 将“关于...”菜单项添加到系统菜单中。

    // IDM_ABOUTBOX 必须在系统命令范围内。
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
    //  执行此操作
    SetIcon(m_hIcon, TRUE);// 设置大图标
    SetIcon(m_hIcon, FALSE);// 设置小图标

#ifdef _DEBUG
    this->GetDlgItem(IDC_BUTTON_API_TEST)->ShowWindow(SW_SHOW);
    this->GetDlgItem(IDOK)->ShowWindow(SW_SHOW);
#else
    this->GetDlgItem(IDC_BUTTON_API_TEST)->ShowWindow(SW_HIDE);
    this->GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
#endif

    ReadSetting();

    if(mSetting.token_size == 0)
    {
        ShowAuthDialog();
    }
    PCS *pcs = new PCS(access_token);
    if( !pcs->mkdir(DEFAULT_ROOT_PATH) )
    {
        CString msg;
        msg.Format(_T("创建目录失败, 请在/我的应用数据/下建立 \"assistant\"目录"));
        MessageBox(msg);
    }
    delete pcs;
    mRoot.info.fs_id = 0;
    mRoot.info.isdir = TRUE;
    mRoot.info.path = L"/apps/assistant";

    mCurrentIndex = 0;

    m_pFont = new CFont();
    m_pFont->CreateFont(
        20,
        0,
        0,
        0,
        FW_NORMAL,
        FALSE,
        FALSE,
        0,
        ANSI_CHARSET,
        OUT_SCREEN_OUTLINE_PRECIS,
        CLIP_DEFAULT_PRECIS,
        DEFAULT_QUALITY,
        DEFAULT_PITCH | FF_SWISS,
        USE_FONT_NAME );
    ((CEdit *)GetDlgItem(IDC_EDIT_PRE_PATH))->SetFont(m_pFont);
    ((CEdit *)GetDlgItem(IDC_EDIT_SUFF_PATH))->SetFont(m_pFont);
    ((CEdit *)GetDlgItem(IDC_EDIT_PRE_PATH))->SetWindowText(_T("/apps/assistant/"));
    ((CEdit *)GetDlgItem(IDC_EDIT_REPLACE_CONTENT))->SetWindowText(_T("1"));
    return TRUE; // 除非将焦点设置到控件，否则返回 TRUE
}

void CMainDialog::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialog::OnSysCommand(nID, lParam);
    }
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMainDialog::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // 用于绘制的设备上下文

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 使图标在工作矩形中居中
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 绘制图标
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CMainDialog::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

BOOL CMainDialog::DestroyWindow()
{
    return CDialog::DestroyWindow();
}

void CMainDialog::OnBnClickedOk()
{
    std::wstring sc = getRandString(12);
    std::wstring ss = getTianGan(27);
    return;
    CWaitCursor c;
    PCS *pcs = new PCS(access_token);
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);

    renameAll(pcs, mRoot, MODE_RENAME);
    int sms = mMoveList.size();
    int smaps = mMatchupRename.size();
    saveMapFile();
    mMatchupRename.clear();
    readMapFile();
    //pcs->move(mMoveList);
    delete pcs;
}

long CMainDialog::countFile(FileTree root)
{
    long count = 0;
    if(root.tree.size() > 0)
    {
        for(std::vector<FileTree>::iterator it = root.tree.begin(); it != root.tree.end(); it++)
        {
            count += countFile(*it);
        }
    }
    else
    {
        ++count;
    }

    return count;
}

std::wstring CMainDialog::genMapFileName(FileTree root)
{
    SYSTEMTIME systemTime;
    ::GetLocalTime(&systemTime);
    CTime cTimeNow(systemTime);
    CString str = cTimeNow.Format("%Y%m%d%H%M%S");
    std::vector<FileTree> tree = root.tree;
    CString strMap;
    strMap.Format(_T("seed_%s.map"), str);
    std::wstring mapFileName = strMap.GetBuffer(0);
    strMap.ReleaseBuffer();
    
    if (tree.size() > 0)
    {
        long count = 0;
        count = countFile(root);

        std::wstring firstName = root.tree[0].info.path;
        if (firstName.find_last_of(L'/') == firstName.size() - 1)
        {
            firstName = firstName.substr(0, firstName.size() - 1);
        }

        std::wstring::size_type pos = firstName.find_last_of(L'/');
        std::wstring prefix = firstName.substr(0, pos + 1);
        firstName = firstName.substr(pos + 1);

        CString strMap;
        strMap.Format(_T("%s等%d个文件.map"), firstName.c_str(), count);
        mapFileName = strMap.GetBuffer(0);
    }

    return mapFileName;
}

std::wstring CMainDialog::getFileNameByPath(std::wstring path, BOOL leftBias)
{
    wchar_t _bias = leftBias ? L'\\' : L'/';
    std::wstring firstName = path;
    if (firstName.find_last_of(_bias) == firstName.size() - 1)
    {
        firstName = firstName.substr(0, firstName.size() - 1);
    }

    std::wstring::size_type pos = firstName.find_last_of(_bias);
    std::wstring prefix = firstName.substr(0, pos + 1);
    firstName = firstName.substr(pos + 1);

    return firstName;
}

void CMainDialog::saveMapFile(std::wstring mapFileName)
{
    mapFileName = getFileNameByPath(mapFileName, TRUE);
    memset( &m_sMapFileName, 0x00, sizeof(m_sMapFileName));
    wsprintf(m_sMapFileName, _T("%s"), mapFileName.c_str());
    CFile mFile(mapFileName.c_str(), CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate);

    /* 写入文件名 */
    mFile.Write(&m_sMapFileName, sizeof(m_sMapFileName));
    for (std::map<std::wstring, std::wstring>::const_iterator it = mMatchupRename.begin(); it != mMatchupRename.end(); it++)
    {
        MapItem item;
        memset(&item, 0x00, sizeof(MapItem));
        memcpy(item.key, it->first.c_str(), it->first.size() * sizeof(std::wstring::value_type));
        memcpy(item.value, it->second.c_str(), it->second.size() * sizeof(std::wstring::value_type));
        mFile.Write(&item, sizeof(MapItem));
    }
    mFile.Close();
}
void CMainDialog::readMapFile(std::wstring mapFileName)
{
    CFile mFile(mapFileName.c_str(), CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate);
    MapItem item;
    memset(&item, 0x00, sizeof(MapItem));

    /* 读取文件名 */
    memset( &m_rMapFileName, 0x00, sizeof(m_rMapFileName));
    mFile.Read(&m_rMapFileName, sizeof(m_rMapFileName));
    while (sizeof(MapItem) == mFile.Read(&item, sizeof(MapItem)))
    {
        insertToMap(std::wstring(item.key), std::wstring(item.value), MODE_RECOVER);
        memset(&item, 0x00, sizeof(MapItem));
    }
    mFile.Close();
}

std::wstring CMainDialog::getCurrentIndexString(long &index)
{
    CString form;
    m_sMarkName = getRandString(time(NULL));
    form.Format(_T("%s_%s.cpp"), m_sMarkName.c_str(), getTianGan(++mCurrentIndex).c_str());
    index = mCurrentIndex;

    return std::wstring(form.GetBuffer(0));
}

void CMainDialog::insertToMap(std::wstring key, std::wstring value, OpMode mode)
{
    switch (mode)
    {
    case MODE_RENAME:
        mMatchupRename.insert(make_pair(key, value));
        break;
    case MODE_RECOVER:
        mMatchupRecover.insert(make_pair(key, value));
        break;
    default:
        break;
    }
}

std::wstring CMainDialog::constructNewName(PCS *pcs, std::wstring old, std::wstring &old_name, std::wstring &new_name, OpMode mode)
{
    std::wstring newName;
    if (old.find_last_of(L'/') == old.size() - 1)
    {
        old = old.substr(0, old.size() - 1);
    }

    std::wstring::size_type pos = old.find_last_of(L'/');
    std::wstring prefix = old.substr(0, pos + 1);
    old_name = old.substr(pos + 1);
    long index = -1;
    switch (mode)
    {
    case MODE_RENAME:
        new_name = getCurrentIndexString(index);
        break;
    case MODE_RECOVER:
        {
            std::map<std::wstring, std::wstring>::const_iterator it = mMatchupRecover.find(old_name);
            if (it == mMatchupRecover.end())
            {
                new_name = old_name;
            }
            else
            {
                new_name = it->second;
            }
        }
        break;
    case MODE_ADD_TAIL:
        new_name = old_name;
        new_name.append(pcs->mTail);
        break;
    case MODE_REMOVE_TAIL:
        new_name = old_name;
        {
            std::wstring regexString = L"(.*)";
            regexString.append(pcs->mTail);
            regexString.append(L"$");
            boost::wregex reg(regexString, boost::regex::icase); 
            std::wstring::const_iterator itor = old_name.begin();
            std::wstring::const_iterator end_itor = old_name.end();
            boost::wsmatch mc;

            if(boost::regex_search(itor, end_itor, mc, reg)) {
                new_name = mc[1];
            }
        }
        break;
    default:
        break;
    }
    newName = prefix.append(new_name);

    return newName;
}

void CMainDialog::renameAll(PCS *pcs, FileTree current, OpMode mode)
{
    if (current.tree.size() > 0)
    {
        std::vector<FileTree>::const_iterator it = current.tree.begin();
        std::vector<FileTree>::const_iterator end = current.tree.end();
        for (; it != end; it++)
        {
            renameAll(pcs, *it, mode);
        }
    }
    if (current.info.fs_id != 0)
    {
        std::wstring old_path = current.info.path;
        std::wstring new_path;
        std::wstring new_name;
        std::wstring old_name;

        if(mode == MODE_ADD_TAIL || mode == MODE_REMOVE_TAIL )
        {
            if(current.info.isdir)
            {
                return;
            }
        }

        new_path = constructNewName(pcs, old_path, old_name, new_name, mode);

        MoveItem item;
        item.from = StringUtil::wstring2string(StringUtil::URLEncoding(old_path));
        item.to = StringUtil::wstring2string(StringUtil::URLEncoding(new_path));
        mMoveList.push_back(item);
        if (mode == MODE_RENAME)
        {
            insertToMap(new_name, old_name, mode);
        }
    }
}

void CMainDialog::walkdir(PCS *pcs, std::wstring dir, FileTree &current)
{
    std::vector < File > filelst;
    pcs->list(dir, filelst);
    for (std::vector<File>::const_iterator it = filelst.begin(); it != filelst.end(); it++)
    {
        FileTree _curr;
        _curr.info = *it;

        if (it->isdir)
        {
            walkdir(pcs, it->path, _curr);
        }
        else
        {
            mFileList.push_back(*it);
        }
        current.tree.push_back(_curr);
    }
}

void CMainDialog::ReadSetting(void)
{
    CFile mFile(_T("user.dat"), CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate);
    memset(&mSetting, 0x00, sizeof(Setting));
    mFile.Read(&mSetting, sizeof(Setting));
    mFile.Close();
    access_token = StringUtil::string2wstring(std::string(mSetting.token));
}

void CMainDialog::WriteSetting(void)
{
    CFile mFile(_T("user.dat"), CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate);
    memset(&mSetting, 0x00, sizeof(Setting));
    mSetting.token_size = access_token.length();
    strcpy_s(mSetting.token, StringUtil::wstring2string(access_token).c_str());
    mFile.Write(&mSetting, sizeof(Setting));
    mFile.Close();
}

void CMainDialog::ShowAuthDialog(void)
{
    HeiduDlg dlg;
    dlg.DoModal();
    access_token = dlg.access_token;
    if (access_token.length() < 10)
    {
        MessageBox(_T("无效的access_token"));
        return;
    }
    WriteSetting();
}

void CMainDialog::OnBnClickedButtonChangeAccount()
{
    ShowAuthDialog();
}

void CMainDialog::OnBnClickedButtonApiTest()
{
    CWaitCursor c;
    PCS *pcs = new PCS(access_token);
    mRoot.tree.clear();
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);
    delete pcs;
    m_TreeFile.setFileTreeData(mRoot);
    return;
    Quota quota;
    pcs->quota(quota);
    std::vector < File > filelst;
    pcs->list(L"/apps/assistant/hello/深空天体", filelst);

    std::vector < MoveItem > itemLst;
    MoveItem item;
    item.to = StringUtil::wstring2string(StringUtil::URLEncoding(L"/apps/assistant/hello/深空天体/深空天体/猎户座.jpg"));
    item.from = StringUtil::wstring2string(StringUtil::URLEncoding(L"/apps/assistant/hello/深空天体/深空天体/_______1.j"));
    itemLst.push_back(item);
    BOOL status = pcs->move(itemLst);
    status = pcs->move("/apps/assistant/hello1", "/apps/assistant/hello");
    long errorCode = pcs->getErrorCode();
    std::wstring errorMsg = pcs->getErrorMsg();

    delete pcs;
}

void CMainDialog::OnBnClickedButtonRename()
{
    CWaitCursor c;

    mRoot.tree.clear();
    PCS *pcs = new PCS(access_token);
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);

    m_sMarkName = getRandString(time(NULL));

    CString defaultDir = L".\\";
    CString fileName = genMapFileName(mRoot).c_str();
    CString filter = L"Map文件 (*.map;)|*.map||";   //文件过虑的类型  
    CFileDialog openFileDlg(FALSE, defaultDir, fileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, filter, NULL);  
    openFileDlg.GetOFN().lpstrInitialDir = defaultDir + fileName;//L".\\user.map";  
    INT_PTR result = openFileDlg.DoModal();  
    CString filePath = defaultDir + "\\" + fileName;  
    if(result == IDOK) {  
        filePath = openFileDlg.GetPathName();

        CWaitCursor c;
        mMoveList.clear();
        mMatchupRename.clear();
        renameAll(pcs, mRoot, MODE_RENAME);
        int sms = mMoveList.size();
        int smaps = mMatchupRename.size();
        pcs->move(mMoveList);
        saveMapFile(std::wstring(filePath.GetBuffer(0)));
        ::PostMessage(this->m_hWnd, UM_UPDATETREE, NULL, NULL);
    }
    delete pcs;
}

void CMainDialog::OnBnClickedButtonRecover()
{
    CFileDialog dlgFile(TRUE, NULL ,NULL, OFN_HIDEREADONLY, _T("Map Files(*.map)|*.map;||"), NULL);
    if (dlgFile.DoModal() == IDOK)
    {
        CWaitCursor c;

        mMatchupRecover.clear();
        CString m_strFilePath = dlgFile.GetPathName();
        readMapFile(std::wstring(m_strFilePath.GetBuffer(0)));
        GetDlgItem(IDC_EDIT_SUFF_PATH)->SetWindowText(m_rMapFileName);
        ::PostMessage(this->m_hWnd, UM_RECOVER, NULL, NULL);
    }
}

LRESULT CMainDialog::OnRecover(WPARAM wParam, LPARAM lParam)
{
    CWaitCursor c;
    PCS *pcs = new PCS(access_token);
    mRoot.tree.clear();
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);
    mMoveList.clear();
    renameAll(pcs, mRoot, MODE_RECOVER);
    int sms = mMoveList.size();
    int smaps = mMatchupRename.size();
    pcs->move(mMoveList);
    delete pcs;
    ::PostMessage(this->m_hWnd, UM_UPDATETREE, NULL, NULL);
    return 0;
}

LRESULT CMainDialog::OnUpdateTree(WPARAM wParam, LPARAM lParam)
{
    CWaitCursor c;
    PCS *pcs = new PCS(access_token);
    mRoot.tree.clear();
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);
    delete pcs;
    if (mRoot.tree.size() == 0)
    {
        FileTree ft;
        ft.info.path = L"没有任何内容";
        ft.info.isdir = TRUE;
        mRoot.tree.push_back(ft);
    }
    m_TreeFile.setFileTreeData(mRoot);
    return 0;
}

void CMainDialog::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);

    CRect r;
    GetClientRect(&r);
    r.top += 93;
#if _DEBUG
    r.bottom -= 50;
#else
    r.bottom -= 10;
#endif
    r.left += 11;
    r.right -= 11;
    m_TreeFile.MoveWindow(r);
}

int CMainDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDialog::OnCreate(lpCreateStruct) == -1)
        return -1;

    CRect r;
    if (!m_TreeFile.Create(
        WS_VISIBLE | WS_CHILD | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT | TVS_EDITLABELS | TVS_SHOWSELALWAYS, r, this,
        100))
        return -1;
    //Display the folder items in the tree
    FileTree file;
    file.info.path = L"/apps/assistant/请先刷新显示";
    file.info.isdir = 1;

    FileTree file1;
    file1.info.isdir = true;
    file1.info.path = L"/hi/方可显示目录结构";

    FileTree file2;
    file2.info.isdir = true;
    file2.info.path = L"/hi/second/third";

    FileTree file3;
    file3.info.isdir = true;
    file3.info.path = L"/hi/second/fourth";

    FileTree file4;
    file4.info.isdir = true;
    file4.info.path = L"/hi/second/fifth";

    FileTree file5;
    file5.info.isdir = false;
    file5.info.path = L"/hi/second/fifth/six.exe";

    file4.tree.push_back(file5);
    file3.tree.push_back(file4);
    file2.tree.push_back(file3);
    file1.tree.push_back(file2);
    file.tree.push_back(file1);
    mRoot.tree.push_back(file);
    m_TreeFile.setFileTreeData(mRoot);
    return 0;
}

void CMainDialog::OnBnClickedButtonRefresh()
{
    ::PostMessage(this->m_hWnd, UM_UPDATETREE, NULL, NULL);
}

void CMainDialog::OnBnClickedButtonAddTail()
{
    CString content;
    ((CEdit *)GetDlgItem(IDC_EDIT_REPLACE_CONTENT))->GetWindowText(content);
    if( content.GetLength() <= 0 )
    {
        this->MessageBox(L"内容不能为空!", L"提示");
        return;
    }

    CWaitCursor c;

    mRoot.tree.clear();
    PCS *pcs = new PCS(access_token);
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);

    mMoveList.clear();
    mMatchupRename.clear();
    pcs->mTail = std::wstring(content.GetBuffer(0));
    renameAll(pcs, mRoot, MODE_ADD_TAIL);
    int sms = mMoveList.size();
    int smaps = mMatchupRename.size();
    pcs->move(mMoveList);
    ::PostMessage(this->m_hWnd, UM_UPDATETREE, NULL, NULL);

    delete pcs;
}

void CMainDialog::OnBnClickedButtonRemoveTail()
{
    CString content;
    ((CEdit *)GetDlgItem(IDC_EDIT_REPLACE_CONTENT))->GetWindowText(content);
    if( content.GetLength() <= 0 )
    {
        this->MessageBox(L"内容不能为空!", L"提示");
        return;
    }
    CWaitCursor c;

    mRoot.tree.clear();
    PCS *pcs = new PCS(access_token);
    walkdir(pcs, DEFAULT_ROOT_PATH, mRoot);

    mMoveList.clear();
    mMatchupRename.clear();
    pcs->mTail = std::wstring(content.GetBuffer(0));
    renameAll(pcs, mRoot, MODE_REMOVE_TAIL);
    int sms = mMoveList.size();
    int smaps = mMatchupRename.size();
    pcs->move(mMoveList);
    ::PostMessage(this->m_hWnd, UM_UPDATETREE, NULL, NULL);

    delete pcs;
}

void CMainDialog::OnBnClickedButtonBlank()
{

}

void CMainDialog::OnBnClickedButtonHelp()
{
    CHelpDlg dlgAbout;
    dlgAbout.DoModal();
}

void CMainDialog::OnEnChangeEditInput()
{
    CString content;
    ((CEdit *)GetDlgItem(IDC_EDIT_REPLACE_CONTENT))->GetWindowText(content);
    std::wstring szContent = std::wstring(content.GetBuffer(0));
    CString targatText(L"");
    targatText.Format(L"在末尾添加\"%s\"", szContent.c_str());
    ((CButton *)GetDlgItem(IDC_BUTTON_ADD_TAIL))->SetWindowText(targatText);
    targatText.Format(L"去掉末尾的\"%s\"", szContent.c_str());
    ((CButton *)GetDlgItem(IDC_BUTTON_REMOVE_TAIL))->SetWindowText(targatText);
}
