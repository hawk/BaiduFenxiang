#ifndef  __TYPES_H__
#define __TYPES_H__

#include "PCS.h"

typedef struct _Setting {
    size_t token_size;
    char token[512];
} Setting;

class FileTree {
public:
    File info;
    std::vector<FileTree> tree;
};

typedef struct _MapItem {
    wchar_t key[256];
    wchar_t value[256];
} MapItem;

typedef enum _OpMode {
    MODE_RENAME,
    MODE_RECOVER,
    MODE_ADD_TAIL,
    MODE_REMOVE_TAIL
} OpMode;

#endif /* __TYPES_H__ */