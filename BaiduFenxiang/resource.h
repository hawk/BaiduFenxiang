//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Heidu.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_Heidu_DIALOG                102
#define IDD_AUTH_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDR_HTML1                       136
#define IDR_HTML2                       137
#define IDD_MAIN_DIALOG                 138
#define IDD_HELPBOX                     139
#define IDC_EXPLORER1                   1004
#define IDC_IPADDRESS1                  1005
#define IDC_BUTTON_CHANGE_ACCOUNT       1006
#define IDC_BUTTON_API_TEST             1007
#define IDC_BUTTON_RENAME               1008
#define IDC_BUTTON_RECOVER              1009
#define IDC_EDIT_PRE_PATH               1010
#define IDC_EDIT_SUFF_PATH              1011
#define IDC_BUTTON_CHANGE_ACCOUNT2      1012
#define IDC_BUTTON_REFRESH              1012
#define IDC_BUTTON_ADD_TAIL             1013
#define IDC_BUTTON_REMOVE_TAIL          1014
#define IDC_BUTTON_HELP                 1015
#define IDC_EDIT_REPLACE_CONTENT        1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
