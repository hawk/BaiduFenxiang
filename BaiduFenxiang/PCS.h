#pragma once
#include "StringUtil.h"
#include "WinHttpClient.h"
#include "json/json.h"
#include "Utils.h"

class Quota {
public:
    long long quota;
    long long used;
};
class File {
public:
    long long fs_id;
    std::wstring path;
    long size;
    BOOL isdir;
};

class MoveItem {
public:
    std::string from;
    std::string to;
};

#define get_value_string( _target, _field, _container ) \
{\
Json::Value __##_field = _container[#_field];\
if( __##_field.isNull())\
{\
    _target._field = L"";\
}\
else\
{\
    _target._field = StringUtil::string2wstring(__##_field.asString());\
}\
}

#define get_value_long( _target, _field, _container ) \
{\
Json::Value __##_field = _container[#_field];\
if( __##_field.isNull())\
{\
    _target._field = 0;\
}\
else\
{\
    _target._field = __##_field.asDouble();\
}\
}

#define get_value_bool( _target, _field, _container ) \
{\
Json::Value __##_field = _container[#_field];\
if( __##_field.isNull())\
{\
    _target._field = FALSE;\
}\
else\
{\
    _target._field = __##_field.asBool();\
}\
}

class PCS
{
public:
    std::wstring mAccessToken;
    WinHttpClient mHttpClient;
    std::wstring mResContent;
    long mErrorCode;
    std::wstring mErrorMsg;
    std::wstring mTail;
public:
    PCS(void)
    {
        mErrorCode = 0;
        mErrorMsg = L"";
    }
    PCS(std::wstring token)
    {
        PCS();
        this->mAccessToken = token;
    }
    ~PCS(void)
    {

    }
    long getErrorCode()
    {
        return mErrorCode;
    }
    std::wstring getErrorMsg()
    {
        return mErrorMsg;
    }
protected:
    void resetStatus()
    {
        mErrorCode = 0;
        mErrorMsg = L"";
    }
    BOOL get(std::wstring url)
    {
        mHttpClient.UpdateUrl(url);
        if (!mHttpClient.SendHttpRequest())
        {
            return FALSE;
        }
        mResContent = mHttpClient.GetResponseContent();
        mResContent = UnicodeNormalize(mResContent);
        
        return TRUE;
    }

    BOOL post(std::wstring url, std::string data)
    {
        mHttpClient.UpdateUrl(url);
        mHttpClient.SetAdditionalDataToSend((BYTE *)data.c_str(), data.size());

        wstring headers = L"\
Content-type: application/x-www-form-urlencoded\r\n\
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\n\
Connection: keep-alive\r\n\
Content-Length: %d\r\n\r\n";

	    wchar_t szHeaders[MAX_PATH * 10] = L"";
	    swprintf_s(szHeaders, MAX_PATH * 10, headers.c_str(), data.size());
        mHttpClient.SetAdditionalRequestHeaders(szHeaders);
        if (!mHttpClient.SendHttpRequest(L"POST", true))
        {
            return FALSE;
        }

        mResContent = mHttpClient.GetResponseContent();
        mResContent = UnicodeNormalize(mResContent);

        return TRUE;
    }
    std::wstring _x2wchar_t(boost::match_results<std::wstring::const_iterator> match) const
    {
        int x = 1;
        std::wstring _1 = match[1];
        long number = Utils::string2numberEx<long, std::wstring>( _1, 16 );
        std::wstring temp(L"");
        temp += (wchar_t)number;

        return temp;
    }
    std::wstring UnicodeNormalize(std::wstring unicodeString)
    {
        std::wstring szOutputString(unicodeString.c_str());
        try
        {
            /*
            * 处理\x963f, \u963f格式转换为unicode字符串
            */
            boost::wregex reg_re2(L"(?:\\\\x|%|\\\\u)([0-9a-fA-F]{0,4})",boost::regex::icase);
            boost::function<std::wstring (boost::match_results<std::wstring::const_iterator>)> function2 =
                boost::bind(&PCS::_x2wchar_t, this, _1);
            szOutputString = regex_replace(szOutputString, reg_re2, function2);
        }
        catch (std::exception &e)
        {
            TRACE("%s\n", e.what());
            printf("%s\n", e.what());
            szOutputString = unicodeString;
        }

        return szOutputString;
    }
public:
    BOOL quota(Quota &quota)
    {
        resetStatus();
        std::wstring url = L"https://pcs.baidu.com/rest/2.0/pcs/quota?method=info&access_token=";
        url.append(mAccessToken);

        if( !get(url) )
        {
            return FALSE;
        }

        Json::Reader reader;
        Json::Value root;
        if( !reader.parse(StringUtil::wstring2string(mResContent), root, false))
        {
            return FALSE;
        }
        get_value_long(quota, quota, root);
        get_value_long(quota, used, root);

        return TRUE;
    }

    BOOL mkdir(std::wstring path)
    {
        resetStatus();
        std::wstring url = L"https://pcs.baidu.com/rest/2.0/pcs/file?method=mkdir&access_token=";
        url.append(mAccessToken);
        url.append(L"&path=");
        url.append(path);

        if( !get(url) )
        {
            return FALSE;
        }
        Json::Reader reader;
        Json::Value root;
        if( !reader.parse(StringUtil::wstring2string(mResContent), root, false))
        {
            return FALSE;
        }

        File file;
        get_value_long(file, fs_id, root);
        get_value_string(file, path, root);

        if( file.fs_id == 0 && file.path.size() == 0 )
        {
            Json::Value error_msg = root["error_msg"];
            if(error_msg.asString().compare("file already exists") != 0 )
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    BOOL list(std::wstring path, std::vector<File> &filelst)
    {
        resetStatus();
        filelst.clear();
        std::wstring url = L"https://pcs.baidu.com/rest/2.0/pcs/file?method=list&access_token=";
        url.append(mAccessToken);
        url.append(L"&path=");
        url.append(path);

        if( !get(url) )
        {
            return FALSE;
        }
        Json::Reader reader;
        Json::Value root;
        if( !reader.parse(StringUtil::wstring2string(mResContent), root, false))
        {
            return FALSE;
        }
        Json::Value _list = root["list"];
        if( _list.isNull())
        {
            return FALSE;
        }
        for( size_t i = 0; i < _list.size(); i++)
        {
            Json::Value def;
            Json::Value _file = _list.get(i, def);
            if( _file.isNull() )
            {
                break;
            }
            File file;
            get_value_long(file, fs_id, _file);
            get_value_string(file, path, _file);
            get_value_long(file, size, _file);
            get_value_bool(file, isdir,_file);
            filelst.push_back(file);
        }

        return TRUE;
    }

    BOOL move(std::string from, std::string to)
    {
        MoveItem item;
        item.from = from;
        item.to   = to;
        std::vector<MoveItem> moveList;
        moveList.push_back(item);
        
        return move(moveList);
    }

    BOOL move(std::vector<MoveItem> moveList)
    {
        resetStatus();
        std::wstring post_url = L"https://pcs.baidu.com/rest/2.0/pcs/file?method=move&access_token=";
        post_url.append(mAccessToken);

        std::string header = "param={\"list\":[";
        std::string tail   = "]}";
        std::string data = header;

        for( std::vector<MoveItem>::const_iterator it = moveList.begin(); it != moveList.end(); it++)
        {
            std::string element_format = "{\"from\":\"";
            element_format.append(it->from);
            element_format.append("\",\"to\":\"");
            element_format.append(it->to);
            element_format.append("\"},");

            data.append(element_format);
        }
        data = data.substr(0, data.size() - 1);
        data.append(tail);

        if( !post(post_url, data) )
        {
            return FALSE;
        }
        Json::Reader reader;
        Json::Value root;
        if( !reader.parse(StringUtil::wstring2string(mResContent), root, false))
        {
            return FALSE;
        }

        Json::Value error_code = root["error_code"];
        Json::Value error_msg  = root["error_msg"];

        if ( !error_code.isNull() && !error_msg.isNull() )
        {
            mErrorCode = (long)error_code.asDouble();
            mErrorMsg  = StringUtil::string2wstring(error_msg.asString());
            return FALSE;
        }
        
        return TRUE;
    }

};
