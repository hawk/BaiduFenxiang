#include "stdafx.h"

#include <iostream>
#include <string>
#include <sstream>
#include <comutil.h>
#include <icrsint.h>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include "StringUtil.h"

#pragma warning(push)
#pragma warning(disable: 4127)

#ifdef _DEBUG
#pragma  comment(lib,"comsuppwd.lib")
#else
#pragma  comment(lib,"comsuppw.lib")
#endif

std::wstring StringUtil::getFileName(std::wstring old)
{
    std::wstring newName;
    if( old.find_last_of(L'/') == old.size() - 1 )
    {
        old = old.substr(0, old.size() -1);
    }

    std::wstring::size_type pos = old.find_last_of(L'/');
    newName = old.substr(pos + 1);

    return newName;
}

int StringUtil::string2int(std::string str)
{
    int value = 0;
    try
    {
        value = boost::lexical_cast<int>(str);
    }
    catch (const boost::bad_lexical_cast& e)
    {
        printf("%s\n", e.what());
    }

    return value;
}

std::wstring StringUtil::double2wstring(double number)
{
    return boost::lexical_cast<std::wstring>(number);
}

std::string StringUtil::double2string(double number)
{
    return boost::lexical_cast<std::string>(number);
}

double StringUtil::string2double(std::string str)
{
    double value = 0.0f;
    try
    {
        value = boost::lexical_cast<double>(str);
    }
    catch (const boost::bad_lexical_cast& e)
    {
        e.what();
    }

    return value;
}

double StringUtil::wstring2double(std::wstring str)
{
    double value = 0.0f;
    try
    {
        value = boost::lexical_cast<double>(str);
    }
    catch (const boost::bad_lexical_cast& e)
    {
        e.what();
    }

    return value;
}

std::string StringUtil::long2string(long number)
{
    return boost::lexical_cast<std::string>(number);
}

std::wstring StringUtil::long2wstring(long number)
{
    return boost::lexical_cast<std::wstring>(number);
}

std::string StringUtil::wstring2string(std::wstring szWstring)
{
	if (szWstring.empty())
		return string("");
	const wchar_t* pwchar = szWstring.c_str();
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwchar, -1, NULL, 0, NULL, NULL);
	if (nLen<= 0) return std::string("");
	char* pszDst = new char[nLen];
	if (NULL == pszDst) return std::string("");
	WideCharToMultiByte(CP_ACP, 0, pwchar, -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen -1] = 0;
	std::string strTemp(pszDst);
	delete [] pszDst;
	return strTemp;
}

std::wstring StringUtil::string2wstring(std::string szString)
{
	if (szString.empty())
		return wstring(L"");
	const char *pchar = szString.c_str();
	int nLen = szString.size();
	int nSize = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)pchar, nLen, 0, 0);
	if(nSize <= 0) return NULL;
	WCHAR *pwszDst = new WCHAR[nSize+1];
	if( NULL == pwszDst) return NULL;
	MultiByteToWideChar(CP_ACP, 0,(LPCSTR)pchar, nLen, pwszDst, nSize);
	pwszDst[nSize] = 0;
	if( pwszDst[0] == 0xFEFF) // skip Oxfeff
		for(int i = 0; i < nSize; i ++)
			pwszDst[i] = pwszDst[i+1];
	std::wstring wcharString(pwszDst);
	delete pwszDst;
	return wcharString;
}
std::string StringUtil::Unicode2UTF8(std::wstring &szUnicode)
{
	if ( szUnicode.empty())
		return std::string("");
	std::string _szRet = "";
	for (std::wstring::iterator it = szUnicode.begin();it < szUnicode.end();++it)
	{
		if (*it >= 0x0800 && *it <= 0xffff)
		{
			wchar_t _char = *it;
			unsigned char _firstP = ((_char & 0xf000 ) >> 12 ) | 0xe0;
			_szRet.push_back(_firstP);
			_firstP = ((_char & 0x0fc0 ) >> 6) | 0x80;
			_szRet.push_back(_firstP);
			_firstP = (_char & 0x003f ) | 0x80;
			_szRet.push_back(_firstP);
		}
		else
		{
			_szRet.push_back((unsigned char)*it);
		}
	}
	return _szRet;
}

std::wstring StringUtil::GetByRegex(wstring &_sSource, const wstring _sRegex)
{
	wstring _ret = L"";
	try
	{
		boost::wregex reg(_sRegex,boost::regex::icase); 
		std::wstring::const_iterator itor = _sSource.begin();
		std::wstring::const_iterator end_itor = _sSource.end();
		boost::wsmatch mc;

		if ( boost::regex_search(itor, end_itor, mc, reg) )
		{
			_ret = mc[1];
		}
	}
	catch (exception& e)
	{
		_ret = L"";
#ifdef _DEBUG
		printf("%s->%s->%s:%s",__FILE__, __FUNCDNAME__, __LINE__, e.what());
#else
		printf("%s",e.what());
#endif
	}
	return _ret;	
}
bool StringUtil::EraseRegex(std::wstring &_sSource, const std::wstring _sRegex)
{	
	try
	{
		boost::wregex reg(_sRegex,boost::regex::icase); 
		wstring ret = boost::regex_replace(_sSource, reg, L"", boost::match_default | boost::format_sed);
		_sSource = ret;
	}
	catch (std::exception& e)
	{
#ifdef _DEBUG
		printf("%s->%s->%s:%s",__FILE__, __FUNCDNAME__, __LINE__, e.what());
#else
		printf("%s",e.what());
#endif
	}
	return true;	
}
std::wstring StringUtil::Trim(const std::wstring &source, const std::wstring &targets)
{
	wstring::size_type start = 0;
	wstring::size_type end = 0;
	for (start = 0; start < source.size(); start++)
	{
		bool bIsTarget = false;
		for (wstring::size_type i = 0; i < targets.size(); i++)
		{
			if (source[start] == targets[i])
			{
				bIsTarget = true;
				break;
			}
		}
		if (!bIsTarget)
		{
			break;
		}
	}
	for (end = source.size() - 1; (int)end >= 0; end--)
	{
		bool bIsTarget = false;
		for (wstring::size_type i = 0; i < targets.size(); i++)
		{
			if (source[end] == targets[i])
			{
				bIsTarget = true;
				break;
			}
		}
		if (!bIsTarget)
		{
			break;
		}
	}
	wstring result = L"";
	if (end >= start && start < source.size() && end >= 0)
	{
		result = source.substr(start, end-start+1);
	}

	return result;
}
std::wstring StringUtil::ReplaceString(const std:: wstring &srcStr, const std::wstring &oldStr, const std::wstring &newStr)
{
	if (srcStr.size() <= 0 || oldStr.size() <= 0)
	{
		return srcStr;
	}
	wstring strReturn = srcStr;
	wstring::size_type offset = 0;
	wstring::size_type start = strReturn.find(oldStr);
	while (start != wstring::npos)
	{
		offset = start + newStr.size();
		strReturn.replace(start, oldStr.size(), newStr);
		start = strReturn.find(oldStr, offset);
	}

	return strReturn;
}
int StringUtil::StringToInteger(const std::wstring &number)
{
	if (number.size() <= 0)
	{
		return 0;
	}
	wstring num = ReplaceString(number, L",", L"");
	num = ReplaceString(num, L" ", L"");

	return _wtoi(num.c_str());
}

std::wstring StringUtil::LowerString(const wstring &text)
{
	if (text.size() <= 0)
	{
		return L"";
	}
	unsigned int iLength = text.size() + 1;
	wchar_t *pTemp = new wchar_t[iLength];
	if (pTemp == NULL)
	{
		return L"";
	}
	wcscpy_s(pTemp, iLength, text.c_str());
	_wcslwr_s(pTemp, iLength);
	wstring retStr = pTemp;
	delete[] pTemp;

	return retStr;
}

std::wstring StringUtil::UpperString(const std::wstring &text)
{
	if (text.size() <= 0)
	{
		return L"";
	}
	unsigned int iLength = text.size() + 1;
	wchar_t *pTemp = new wchar_t[iLength];
	if (pTemp == NULL)
	{
		return L"";
	}
	wcscpy_s(pTemp, iLength, text.c_str());
	_wcsupr_s(pTemp, iLength);
	wstring retStr = pTemp;
	delete[] pTemp;

	return retStr;
}
std::wstring StringUtil::URLEncoding(const std::wstring &keyword, bool convertToUTF8)
{
	int iLength = 0;
	char *szKeyword = NULL;

	if (convertToUTF8)
	{
		iLength = ::WideCharToMultiByte(CP_UTF8,
			0,
			keyword.c_str(),
			keyword.length(),
			NULL,
			0,
			NULL,
			NULL);
		if (iLength <= 0)
		{
			return L"";
		}

		szKeyword = new char[iLength];
		if (szKeyword == NULL)
		{
			return L"";
		}
		iLength = ::WideCharToMultiByte(CP_UTF8,
			0,
			keyword.c_str(),
			keyword.length(),
			szKeyword,
			iLength,
			NULL,
			NULL);
	}
	else
	{
		string strKeyword = (char *)(_bstr_t)keyword.c_str();
		iLength = (int)strKeyword.length();
		szKeyword = new char[strKeyword.length() + 1];
		strcpy_s(szKeyword, strKeyword.length() + 1, strKeyword.c_str());
	}

	wstring encodedKeyword = L"";
	string strEncodedKeyword = "";
	for (int i = 0; i < iLength; i++)
	{
		unsigned char c = (unsigned char)szKeyword[i];
		char temp[MAX_PATH] = "";
		sprintf_s(temp, MAX_PATH, "%%%2X", c);
		if (temp[1] == ' ')
		{
			temp[1] = '0';
		}
		strEncodedKeyword += temp;
	}
	if (szKeyword != NULL)
	{
		delete[] szKeyword;
	}
	encodedKeyword = (wchar_t *)(_bstr_t)strEncodedKeyword.c_str();
	encodedKeyword = ReplaceString(encodedKeyword, L" ", L"+");

	return encodedKeyword;
}
bool StringUtil::RegexReplace(std::wstring &szSource, std::wstring szRegex, std::wstring szDestString)
{
	boost::wregex reg(szRegex, boost::regex::icase); 
	wstring retString = L"";
	bool retVal = true;
	try
	{
		retString = boost::regex_replace(szSource, reg, szDestString, boost::match_default | boost::format_sed);
	}
	catch (std::exception &e)
	{
		printf("%s\n", e.what());
		retString = L"";
		retVal = false;
	}
	
	szSource = retString;
	return retVal;
}
bool StringUtil::RegexReplace(std::string &szSource, std::string szRegex, std::string szDestString)
{
	boost::regex reg(szRegex, boost::regex::icase); 
	string retString = "";
	bool retVal = true;
	try
	{
		retString = boost::regex_replace(szSource, reg, szDestString, boost::match_default | boost::format_sed);
	}
	catch (std::exception &e)
	{
		printf("%s\n", e.what());
		retString = "";
		retVal = false;
	}

	szSource = retString;
	return retVal;
}
#ifdef _WTL_USE_CSTRING
std::wstring StringUtil::CString2wstring(WTL::CString &szCstring)
{
	return wstring(szCstring.GetBuffer(0));
}
std::string StringUtil::CString2string(WTL::CString &szCstring)
{
	return wstring2string(CString2wstring(szCstring));
}
#endif
std::wstring StringUtil::GetTime(int count)
{
	wchar_t szTimeBuffer[20];
	memset(szTimeBuffer, 0 , 20);
	time_t _time = time(NULL);
	_i64tow_s(_time, szTimeBuffer,20,10);
	wstring res(szTimeBuffer);
	res.append(L"188");
	return res;
}
std::wstring StringUtil::GetModuleFileFullPath()
{
	wchar_t exeFullPath[MAX_PATH];
	memset(exeFullPath,0,sizeof(exeFullPath));
	GetModuleFileName(NULL,exeFullPath,MAX_PATH);//得到程序模块名称，全路径

	return std::wstring(exeFullPath);
}
std::wstring StringUtil::GetTempFilePath()
{
	//
	// /(\w+.(?:gif|jpg|png|bmp|jpeg))
	return std::wstring(L"");
}
std::wstring StringUtil::GetSettingFilePath()
{
	wstring szExePath = StringUtil::GetModuleFileFullPath();
	StringUtil::EraseRegex(szExePath,L"\\\\(\\w+.exe)");
	
	return szExePath;
}