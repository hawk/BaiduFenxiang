#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include <Windows.h>
#include <iostream>
#include <sstream>
#include <comutil.h>
#include <icrsint.h>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>

#include <Windows.h>

#pragma warning(push)
#pragma warning(disable: 4127)

#ifdef _DEBUG
#pragma  comment(lib,"comsuppwd.lib")
#else
#pragma  comment(lib,"comsuppw.lib")
#endif

using namespace std;

namespace Utils
{
    enum CodePage
    {
        CP_DEFAULT = CP_ACP,
        CP_THAI    = 874,
        CP_JPN     = 932,
        CP_GBK     = 936,
        CP_KOR     = 949,
        CP_BIG5    = 950,
        CP_UTF_8   = CP_UTF8,
    };

    template<typename Target, typename Source>
    Target string2numberEx(Source strInput, int _nWeight)
    {
        long value = 0;
        int nWeight = _nWeight;

        for ( Source::size_type i = 0;i < strInput.size(); ++i )
        {

            Source::value_type t_char = strInput.at(i);
            if ( t_char >= L'0' && t_char <= L'9' )
            {
                value = (value * nWeight + ( t_char - L'0') );
            }
            else if ( t_char >= L'a' && t_char <= L'f' )
            {
                value = (value * nWeight + ( t_char - L'a' + 10 ) );
            }
            else if ( t_char >= L'A' && t_char <= L'F' )
            {
                value = (value * nWeight + ( t_char - L'A' + 10 ) );
            }
        }
        return value;
    }

    /************************************************************************/
    /*  完成字符串到数字                                              */
    /************************************************************************/
    template<typename Target, typename Source>
    inline Target string2num(Source str)
    {
        Target value = 0;
        try
        {
            value = boost::lexical_cast<Target>(str);
        }
        catch (const boost::bad_lexical_cast& e)
        {
            printf("%s\n", e.what());
        }

        return value;
    }

    /************************************************************************/
    /*  完成数字到字符串                                              */
    /************************************************************************/
    template<typename Target, typename Source>
    inline Target num2string(Source number)
    {
        return boost::lexical_cast<Target>(number);
    }

    /************************************************************************/
    /* Unicode转CodePage                                                    */
    /************************************************************************/
    inline std::string Unicode2CodePage(const std::wstring szUnicode, CodePage cp = CP_DEFAULT)
    {
        if (szUnicode.empty())
        {
            return string("");
        }
        const wchar_t* pwchar = szUnicode.c_str();
        int nLen = WideCharToMultiByte(cp, 0, pwchar, -1, NULL, 0, NULL, NULL);
        if (nLen<= 0) return std::string("");
        char* pszDst = new char[nLen];
        if (NULL == pszDst) return std::string("");
        WideCharToMultiByte(cp, 0, pwchar, -1, pszDst, nLen, NULL, NULL);
        pszDst[nLen -1] = 0;
        std::string strTemp(pszDst);
        delete [] pszDst;
        return strTemp;
    }

    /************************************************************************/
    /* CodePage转为Unicode                                                  */
    /************************************************************************/
    inline std::wstring CodePage2Unicode(const std::string szCodePage, CodePage cp = CP_DEFAULT)
    {
        if (szCodePage.empty())
        {
            return wstring(L"");
        }
        const char *pchar = szCodePage.c_str();
        int nLen = szCodePage.size();
        int nSize = MultiByteToWideChar(cp, 0, (LPCSTR)pchar, nLen, 0, 0);
        if(nSize <= 0) return NULL;
        WCHAR *pwszDst = new WCHAR[nSize+1];
        if( NULL == pwszDst) return NULL;
        MultiByteToWideChar(cp, 0,(LPCSTR)pchar, nLen, pwszDst, nSize);
        pwszDst[nSize] = 0;
        if( pwszDst[0] == 0xFEFF) // skip Oxfeff
        {
            for(int i = 0; i < nSize; i ++)
            {
                pwszDst[i] = pwszDst[i+1];
            }
        }
        std::wstring wcharString(pwszDst);
        delete pwszDst;
        return wcharString;
    }

    template<typename Target, typename Source>
    inline Target ASCIICopy(Source source)
    {
        Target target;
        target.clear();
        for(Source::size_type i = 0; i < source.length(); i++)
        {
            if( sizeof( Target::value_type ) != sizeof( Source::value_type ) && sizeof( Source::value_type ) == sizeof(char))
            {
                target += (Target::value_type)(unsigned char)source.at(i);
            }
            else
            {
                target += (Target::value_type)source.at(i);
            }
        }
        return target;
    }

    inline std::string wstring2string(std::wstring szWstring)
    {
        return Unicode2CodePage(szWstring);
    }

    inline std::wstring string2wstring(std::string szString)
    {
        return CodePage2Unicode(szString);
    }

    template<typename T>
    inline std::vector<T> getAllMatches(T &_sSource, const T _sRegex)
    {
        std::vector<T> matches;
        boost::basic_regex<T::value_type, boost::regex_traits<T::value_type> > reg(_sRegex, boost::regex::icase);
        boost::find_all_regex(matches, _sSource, reg);

        return matches;
    }

    template<typename T>
    inline bool EraseRegex(T &_sSource, const T _sRegex)
    {
        boost::basic_regex<T::value_type, boost::regex_traits<T::value_type> > reg(_sRegex, boost::regex::icase);
        boost::erase_all_regex(_sSource, reg);

        return true;
    }

    template<typename T>
    inline T Trim(const T &source)
    {
        return boost::trim_copy(source);
    }

    template<typename T>
    inline T ReplaceString(const T &srcStr, const T &oldStr, const T &newStr)
    {
        return boost::replace_all_copy(srcStr, oldStr, newStr );
    }

    template<typename T>
    inline T LowerString(const T &text)
    {
        return boost::to_lower_copy(text);
    }

    template<typename T>
    inline T UpperString(const T &text)
    {
        return boost::to_upper_copy(text);
    }

    inline std::string toURLLike(std::string source)
    {
        int iLength = source.length();
        const std::string::value_type *szKeyword = source.c_str();

        std::string formatedString;
        formatedString.clear();
        for (int i = 0; i < iLength; i++)
        {
            unsigned char c = (unsigned char)szKeyword[i];
            char temp[MAX_PATH] = "";
            if(isalpha(c) || isdigit(c) || c == '-' || c == '.' || c == '~')
            {
                sprintf_s(temp, MAX_PATH, "%c", c);
            }
            else if( c ==' ')
            {
                sprintf_s(temp, MAX_PATH, "+");
            }
            else
            {
                sprintf_s(temp, MAX_PATH, "%%%2X", c);
            }
            formatedString += temp;
        }

        return formatedString;
    }

    inline std::string URLEncoding(const std::wstring &keyaord, CodePage targetEnc = CP_UTF_8 )
    {
        return toURLLike(Unicode2CodePage(keyaord, targetEnc));
    }

    template<typename T>
    inline bool RegexReplace(T &szSource, T szRegex, T szDestString)
    {
        boost::basic_regex<T::value_type, boost::regex_traits<T::value_type> > reg(szRegex, boost::regex::icase);
        T retString;
        retString.clear();
        bool retVal = true;
        try
        {
            retString = boost::regex_replace(szSource, reg, szDestString, boost::match_default | boost::format_sed);
        }
        catch (std::exception &e)
        {
            printf("%s\n", e.what());
            retString.clear();
            retVal = false;
        }

        szSource = retString;
        return retVal;
    }

    inline std::wstring GetModuleFileFullPath()
    {
        std::wstring::value_type exeFullPath[MAX_PATH];
        memset(exeFullPath,0,sizeof(exeFullPath));
        GetModuleFileName(NULL, exeFullPath, MAX_PATH);

        return std::wstring(exeFullPath);
    }
}

#endif /* __UTILS_H__ */