#ifndef  _STRING_UTIL_
#define  _STRING_UTIL_

#include <Windows.h>
#include <string>
#include <iostream>

using namespace std;

class StringUtil
{
public:
    static std::wstring getFileName(std::wstring old);
    static int string2int(std::string str);
    static std::wstring double2wstring(double number);
    static std::string  double2string(double number);
    static double string2double(std::string str);
    static double wstring2double(std::wstring str);
    static std::string long2string(long number);
    static std::wstring long2wstring(long number);
	static std::string wstring2string(std::wstring szWstring);
	static std::wstring string2wstring(std::string szString);
	static std::string Unicode2UTF8(std::wstring &szUnicode);
	static std::wstring GetByRegex(std::wstring &_sSource, const std::wstring _sRegex);
	static bool EraseRegex(std::wstring &_sSource, const std::wstring _sRegex);
	static std::wstring Trim(const std::wstring &source, const std::wstring &targets);
	static std::wstring ReplaceString(const std:: wstring &srcStr, const std::wstring &oldStr, const std::wstring &newStr);
	static int StringToInteger(const std::wstring &number);
	static std::wstring LowerString(const wstring &text);
	static std::wstring UpperString(const std::wstring &text);
	static std::wstring URLEncoding(const std::wstring &keyword, bool convertToUTF8 = true);
	static bool RegexReplace(std::wstring &szSource, std::wstring szRegex, std::wstring szDestString);
	static bool RegexReplace(std::string &szSource, std::string szRegex, std::string szDestString);
#ifdef _WTL_USE_CSTRING
	static std::wstring CString2wstring(WTL::CString &szCstring);
	static std::string CString2string(WTL::CString &szCstring);
#endif
	static std::wstring GetTime(int count = 13);
	static std::wstring GetModuleFileFullPath();
	static std::wstring GetTempFilePath();
	static std::wstring GetSettingFilePath();
private:
	StringUtil();
};
#endif //_STRING_UTIL_