// HeiduDlg.h : 头文件
//

#pragma once
#include "CWebBrowser2.h"
#include <Mshtml.h>
// HeiduDlg 对话框
class HeiduDlg : public CDHtmlDialog
{
// 构造
public:
	HeiduDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_AUTH_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
public:
	CWebBrowser2 * pBrowser;
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	
public:
	afx_msg void OnBnClickedOk();
public:
	DECLARE_EVENTSINK_MAP()
	DECLARE_DHTML_EVENT_MAP()

public:
	void NavigateComplete2Explorer1(LPDISPATCH pDisp, VARIANT* URL);

public:
    std::wstring   access_token;
};
