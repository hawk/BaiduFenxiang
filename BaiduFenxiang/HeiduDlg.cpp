// HeiduDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Heidu.h"
#include "HeiduDlg.h"
#include "CWebBrowser2.h"
#include <mshtml.h>
#include <comutil.h>
#include <Wininet.h>
 
#pragma comment( lib, "comsupp.lib" )

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const TCHAR *client_id = _T("34KeQSdRS58GFdj6OfVmko1S");
const TCHAR *authUrl = _T("https://openapi.baidu.com/oauth/2.0/authorize?response_type=token&client_id=%s&redirect_uri=oob&scope=netdisk");
const TCHAR *logoutUrl = _T("https://passport.baidu.com/?logout&u=https%%3A%%2F%%2Fopenapi.baidu.com%%2Foauth%%2F2.0%%2Fauthorize%%3Fresponse_type%%3Dtoken%%26client_id%%3D%s%%26redirect_uri%%3Doob%%26scope%%3Dnetdisk");
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框


// HeiduDlg 对话框

HeiduDlg::HeiduDlg(CWnd* pParent /*=NULL*/)
	: CDHtmlDialog(HeiduDlg::IDD, 0, pParent)
	,pBrowser(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void HeiduDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(HeiduDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &HeiduDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// HeiduDlg 消息处理程序

BOOL HeiduDlg::OnInitDialog()
{
	
	CDialog::OnInitDialog();
	
	//CRect dlgRect(0,0,800,600);
	//MoveWindow(dlgRect);

    // 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	SetHostFlags(DOCHOSTUIFLAG_FLAT_SCROLLBAR);

	pBrowser = (CWebBrowser2 *) GetDlgItem(IDC_EXPLORER1);
	CRect rect;
	GetClientRect(rect);
	rect.left += 10;
	rect.top += 10;
	rect.right -= 10;
	rect.bottom -= 10;
	pBrowser->MoveWindow(rect);
    CString target;
    target.Format(authUrl, client_id);
    CString logout;
    logout.Format(logoutUrl, client_id);
    COleVariant sLoc(logout);
    pBrowser->Navigate2(sLoc, NULL, NULL, NULL, NULL);

    int x = StringUtil::string2int("12");

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}


// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void HeiduDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR HeiduDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void HeiduDlg::OnBnClickedOk()
{
	OnOK();
}
BEGIN_EVENTSINK_MAP(HeiduDlg, CDialog)
	ON_EVENT(HeiduDlg, IDC_EXPLORER1, 252, HeiduDlg::NavigateComplete2Explorer1, VTS_DISPATCH VTS_PVARIANT)
END_EVENTSINK_MAP()

BEGIN_DHTML_EVENT_MAP(HeiduDlg)
END_DHTML_EVENT_MAP()

void HeiduDlg::NavigateComplete2Explorer1(LPDISPATCH pDisp, VARIANT* URL)
{
	CString url = pBrowser->get_LocationURL();
    std::wstring token = StringUtil::GetByRegex(std::wstring(url.GetBuffer(0)), L"access_token=([^&]*)");
    if( token.length() > 5 )
    {
        access_token = token;
        this->SendMessage(WM_CLOSE);
    }
}

