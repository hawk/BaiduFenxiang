#ifndef __HTTP_CLIENT_H__
#define __HTTP_CLIENT_H__

#include <string>
#include <comutil.h>

typedef bool (*PROGRESSPROC)(double);
using namespace std;

class HttpClient {
public:
    HttpClient(const wstring &url = L"", PROGRESSPROC progressProc = NULL);
    ~HttpClient();

    bool SendHttpRequest(const wstring &httpVerb = L"GET", bool disableAutoRedirect = false);
    wstring GetResponseHeader(void);
    wstring GetResponseContent(void);
    wstring GetResponseCharset(void);
    wstring GetResponseStatusCode(void);
    wstring GetResponseLocation(void);
    wstring GetRequestHost(void);
    const BYTE *GetRawResponseContent(void);
    unsigned int GetRawResponseContentLength(void);
    unsigned int GetRawResponseReceivedContentLength(void);
    bool SaveResponseToFile(const wstring &filePath);
    wstring GetResponseCookies(void);
    bool SetAdditionalRequestCookies(const wstring &cookies);
    bool SetAdditionalDataToSend(BYTE *data, unsigned int dataSize);
    bool UpdateUrl(const wstring &url);
    bool ResetAdditionalDataToSend(void);
    bool SetAdditionalRequestHeaders(const wstring &additionalRequestHeaders);
    bool SetRequireValidSslCertificates(bool require);
    bool SetProxy(const wstring &proxy);
    DWORD GetLastError(void);
    bool SetUserAgent(const wstring &userAgent);
    bool SetForceCharset(const wstring &charset);
    bool SetProxyUsername(const wstring &username);
    bool SetProxyPassword(const wstring &password);
    bool SetTimeouts(unsigned int resolveTimeout = 0,
        unsigned int connectTimeout = 60000,
        unsigned int sendTimeout = 30000,
        unsigned int receiveTimeout = 30000);

private:
    void* pData;
};

#endif /* __HTTP_CLIENT_H__ */
